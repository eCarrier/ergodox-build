# This document

This is a document detailing my [Ergodox](https://www.ergodox.io/) build which is an open-source DIY ergonomic keyboard.

![Picture of the fully built ergodox](src/images/DSC_0639.JPG)

# Bill of material

There are two main parts to the build itself : the electronic and the case. 

I won't be talking about electronic tools here, but you're going to need the [basics](https://learn.adafruit.com/adafruit-guide-excellent-soldering)
to build the keyboard.

## The electronic

### The PCBs

The first big thing that I had to get for the build was the PCB. You can find the desing on the [Ergodox's github page](https://github.com/Ergodox-io/ErgoDox) 
or buy it online as there are multiple place where you can get one (some are listed on the [Ergodox's Website](https://www.ergodox.io/). 
I wanted to have them manufactured to see what was the whole process of sending the PCB to a manufacturer. The cheapest place was [JLCPCB](https://jlcpcb.com/) 
and shipping was fast and not too expensive.

![The PCB](src/images/DSC_0642.JPG)

The great thing about the design of the ergodox is that it is reversible : when you look at it from one side its the left side of the keyboard
and when you flip it around it's the right side

**INSERT PICTURE SIDE BY SIDE**

![The other side](src/images/DSC_0643.JPG)


### The parts

I got all the parts except the keyboard switches and keycaps on [Digikey](https://www.digikey.ca/). Here are the links to everything I bought there :

* 1x[Teesy 2.0](https://www.digikey.ca/en/products/detail/adafruit-industries-llc/199/6827126?s=N4IgTCBcDaIIwFYwA4C0cCcGDMqByAIiALoC%2BQA). You might have to buy some header pins but the Teensy I've bought came with some.
* 1x[Some wire](https://www.digikey.ca/en/products/detail/tensility-international-corp/30-00872/8346519?s=N4IgTCBcDaICoEYDMAOArAWgRgcgERAF0BfIA) to connect to the TRRS connectors
* 1x[USB 2.0 Cable A Male to Mini B Male](https://www.digikey.ca/en/products/detail/assmann-wsw-components/AK672M-2-1/930235?s=N4IgTCBcDaIIIFECMAWArABgLQDkAiIAugL5A). This is a bit to short, get longer if you can.
* 80x[1N4148 through hole diodes](https://www.digikey.ca/en/products/detail/on-semiconductor/1N4148/458603?s=N4IgTCBcDaIIwDkAsckA4BiBlAtAgIiALoC%2BQA), that's one for every keyboard keys.
* 4x[Through hole TRRS female connectors](https://www.digikey.ca/en/products/detail/cui-devices/SJ-43514/368146?s=N4IgTCBcDaIMIAUC0AWAzAVgIwqQOQBEQBdAXyA)
* 1x[MCP23018-E/SP I/O expander](https://www.digikey.ca/en/products/detail/microchip-technology/MCP23018-E-SP/1999505?s=N4IgTCBcDaILIGEAKYDMAGAjADgLQFEB6AZSVwDkAREAXQF8g)
* 1x[USB mini B plug with short cable](https://www.digikey.ca/en/products/detail/hirose-electric-co-ltd/UX40-MB-5P/597531?s=N4IgTCBcDaIBJgJwFZkFoByAREBdAvkA)
* 3x[Orange T1 leds](https://www.digikey.ca/en/products/detail/lite-on-inc/LTL-4291N/3198548?s=N4IgTCBcDaIIwDYAMBaOBOBAOFA5AIiALoC%2BQA)
* 2x[2.2k Ω resistors](https://www.digikey.ca/en/products/detail/stackpole-electronics-inc/CF14JT2K20/1741321?s=N4IgTCBcDaIMIDECMAWAUgFTAaTABjgwFoA5AERAF0BfIA)
* 3x[220 Ω resistors](https://www.digikey.ca/en/products/detail/yageo/CFR-25JB-52-220R/1295?s=N4IgTCBcDa5gDARQEIGkC0A5AIiAugL5A)
* 1x[1x 0.1 µF ceramic capacitor](https://www.digikey.ca/en/products/detail/vishay-bc-components/K104K10X7RF5UH5/2356879?s=N4IgTCBcDaIEIGEwDZkFYEBUC0A5AIiALoC%2BQA)
* 2x[USB mini B through hole connectors](https://www.digikey.ca/en/products/detail/molex/0548190519/773802?s=N4IgTCBcDaIOoFkCMB2JSCsBaAcgERAF0BfIA)
* 2x[TRRS male connectors](https://www.digikey.ca/en/products/detail/adafruit-industries-llc/1799/7241409?s=N4IgTCBcDaIIwFYwA4C0YAsCDMqByAIiALoC%2BQA)

Here are all the parts soldered on the right side of the pcb, including the
switches :
![Soldered parts 1](src/images/DSC_0649.JPG)
![Soldered parts 2](src/images/DSC_0650.JPG)

Here is the rest :
* For the switch, I've bought some [PCB mount Cherry MX Clear Keyswitch](https://mechanicalkeyboards.com/shop/index.php?l=product_detail&p=594) x 100, 
more than needed as I wanted to have some in case they break. 
* For the keycaps I got [these](https://www.amazon.ca/dp/B07CQ3HHQR/ref=pe_3034960_233709270_TE_item). 
* [Wrist Pads](https://www.amazon.ca/dp/B01HQXD5OG/ref=pe_3034960_236394800_TE_dp_1) 
* [Cabinet Bumper](https://www.amazon.ca/100Pack-Self-Adhesive-Silicon-Bumpers-Furniture/dp/B077GCYZFV/ref=sr_1_1?dchild=1&ie=UTF8&keywords=door%20bumper%20damping%20diameter%201%2F2%2C%209.5%20x%203.8&language=en_CA&qid=1595098612&sr=8-1) 
to prevent the case from sliding around my desk
* Last but not least, you'll need [Brass Inserts like
  these](https://www.amazon.ca/dp/B01IYWTCWW/ref=pe_3034960_236394800_TE_dp_2)
and some [m3x6mm bolt
socket](https://www.amazon.ca/dp/B07TDGXN8B/ref=pe_3034960_236394800_TE_dp_4)
to close everything up

Here are the inserts in the top of the case :

![Inserts](src/images/DSC_0651.JPG)

## The case

First I wanted to laser cut the case in acrylic sheets but the existing options online are quite expensive so I've decided to 
go for the 3D printed option (I have access to a 3D printer). After trying a few different models, the one from [AGuyFromOhio](https://www.thingiverse.com/thing:2533064) 
is the best I've found. It's crazy sturdy and the printing went very well.

![The different parts of the case](src/images/DSC_0647.JPG)

# Setting up the keyboard layout

When I was done with actually building the thing, it was time to configure it. IMO the best option for that is the toolchain provided by 
Ergodox EZ ([Oryx](https://ergodox-ez.com/pages/oryx) to configure the layout and [Wally](https://ergodox-ez.com/pages/wally) to upload).

# Notes and lessons I've learned

* I'm going to build another one soon so I might post a more detailed tutorial here.
* Be sure that the TRRS connectors are soldered on the good side of the pcb. I've burned an I/O expander because the one of the left side was
not correctly oriented.
* This was a lenghtly project (started to work on it in February and was done in May) but it was quite fun.
* Typing on an ergodox is amazing but it kinda ruined my muscle memory on standart QWERTY keyboards

